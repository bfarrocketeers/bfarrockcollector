package nuno_velosa.group9_pms;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class RoomAdapter extends BaseAdapter {
    private ArrayList<String> mListItems; // nome do jogador
    private ArrayList<Integer> mListItems2; // id do jogador
    private ArrayList<String> mListItems3; // imagem do jogador
    private LayoutInflater mLayoutInflater;
    private Context ctx;
    private int version;
    private ArrayList<Integer> playerId; // todos os jogadores do jogo (exceto o próprio se estiver individuais, pois aí, terá de alterar a cor do texto em baixo)
    private ArrayList<Integer> colors; // 20 cores que serão atribuidas aos jogadores (para circular os beacons encontrados)
    private int teamChoose;


    ImageLoader imgLoader;



    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;



    public  RoomAdapter(Context context, int userId, ArrayList<String> arrayList, ArrayList<Integer> arrayList2, ArrayList<String> arrayList3, int v, ArrayList<Integer> playerId){

        importantUserId = userId;
        mListItems = arrayList; // nome do jogador
        mListItems2 = arrayList2; // id do jogador
        mListItems3 = arrayList3; // imagem do jogador
        version = v;
        this.playerId = playerId;
        ctx = context;

        imgLoader = new ImageLoader(context);

        colors = new ArrayList<Integer>();

        //get the layout inflater
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        //getCount() represents how many items are in the list
        return mListItems.size();
    }

    @Override
    //get the data of an item from a specific position
    //i represents the position of the item in the list
    public Object getItem(int i) {
        return null;
    }

    @Override
    //get the position id of the item from the list
    public long getItemId(int i) {
        return 0;
    }



    @Override

    public View getView(final int position, View view, ViewGroup viewGroup) {

        // create a ViewHolder reference
        ViewHolder holder;

        //check to see if the reused view is null or not, if is not null then reuse it
        if (view == null) {
            holder = new ViewHolder();

            // Id não fica aqui, porque apenas é guardado, não é mostrado
            if (version == 1) { // Rooms
                view = mLayoutInflater.inflate(R.layout.room_item, null); // room_item é o layout em que estão os items pertencentes à ListView (ou seja, a estrutura de cada linha)
            } else if (version == 2) { // Game (retirar caixa de texto "nada" pois esse espaço é necessário)
                view = mLayoutInflater.inflate(R.layout.game_player_item, null); // room_item é o layout em que estão os items pertencentes à ListView (ou seja, a estrutura de cada linha)
            }
            holder.itemPlayerName = (TextView) view.findViewById(R.id.player_name); // Id do textview que será substituido pelo nome do jogador
            holder.itemPlayerImage = (ImageView) view.findViewById(R.id.player_image); // Id do imageview que será substituido pela imagem do jogador


            if (version == 2) {

                colors.add(R.color.color0);
                colors.add(R.color.color1);
                colors.add(R.color.color2);
                colors.add(R.color.color3);
                colors.add(R.color.color4);
                colors.add(R.color.color5);
                colors.add(R.color.color6);
                colors.add(R.color.color7);
                colors.add(R.color.color8);
                colors.add(R.color.color9);
                colors.add(R.color.color10);
                colors.add(R.color.color11);
                colors.add(R.color.color12);
                colors.add(R.color.color13);
                colors.add(R.color.color14);
                colors.add(R.color.color15);
                colors.add(R.color.color16);
                colors.add(R.color.color17);
                colors.add(R.color.color18);
                colors.add(R.color.color19);

                // Para mudar a cor da border onde estão os scores
                int color = 0;
                for (int i=0; i<playerId.size(); i++) {
                    color = colors.get(playerId.get(i));


                    Drawable background = holder.itemPlayerName.getBackground();
                    if (background instanceof ShapeDrawable) {
                        // cast to 'ShapeDrawable'
                        ShapeDrawable shapeDrawable = (ShapeDrawable) background;
                        shapeDrawable.getPaint().setColor(ContextCompat.getColor(ctx, color));
                    } else if (background instanceof GradientDrawable) {
                        // cast to 'GradientDrawable'
                        GradientDrawable gradientDrawable = (GradientDrawable) background;
                        gradientDrawable.setColor(ContextCompat.getColor(ctx, color));
                        // gradientDrawable.setStroke(24, color);
                    } else if (background instanceof ColorDrawable) {
                        // alpha value may need to be set again after this call
                        ColorDrawable colorDrawable = (ColorDrawable) background;
                        colorDrawable.setColor(ContextCompat.getColor(ctx, color));
                    }

                }




            }


            // the setTag is used to store the data within this view
            view.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)view.getTag();
        }

        //get the string item from the position "position" from array list to put it on the TextView
        String stringItem = mListItems.get(position); // A variável stringItem fica com o nome do jogador do item da ArrayList.
        // Integer stringItem2 = mListItems2.get(position); // A variável stringItem2 fica com o id do jogador do item da ArrayList
        String stringItem3 = mListItems3.get(position); // A variável stringItem3 fica com a imagem do jogador do item da ArrayList.


        if (stringItem != null) { // verifica se não está vazio o campo room name da ArrayList stringItem
            if (holder.itemPlayerName != null) {
                //set the item name on the TextView
                holder.itemPlayerName.setText(stringItem);
            }
        }



        if (stringItem3 != null) { // o link da imagem
            if (holder.itemPlayerImage != null) {
                //set the item name on the TextView
                // holder.itemPlayerImage.setImageResource(stringItem3);
                imgLoader.DisplayImage(stringItem3, R.drawable.loader, holder.itemPlayerImage);
            }
        }


        //this method must return the view corresponding to the data at the specified position.
        return view;

    }

    /**
     * Used to avoid calling of "findViewById" every time the getView() method is called,
     * because this can impact to your application performance when your list is large
     */
    private class ViewHolder {

        protected TextView itemPlayerName;
        protected ImageView itemPlayerImage;
    }

}