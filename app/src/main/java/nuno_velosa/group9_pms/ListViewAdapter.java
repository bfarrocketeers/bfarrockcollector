package nuno_velosa.group9_pms;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;


public class ListViewAdapter extends BaseAdapter {
    private ArrayList<String> mListItems; // nome do quarto
    private ArrayList<String> mListItems2; // tipo de jogo
    private ArrayList<Integer> mListItems3; // imagem para entrar no quarto
    private ArrayList<String> mListItems4; // número de pessoas
    private ArrayList<Integer> mListItems5; // id do quarto (identificação única)
    private ArrayList<String> mListItems6; // password do quarto ("" caso não haja)
    private ArrayList<Integer> mListItems7; // dono do quarto
    private LayoutInflater mLayoutInflater;
    private Context ctx;
    private int teamChoose;


    // Variáveis para aceder à base de dados
    InputStream is=null;
    String result=null;
    String line=null;
    int success;


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    // Variável para parar de atualizar os quartos (é enviada no construtor porque pertence aos Rooms)
    Timer roomsTimer;



    public ListViewAdapter(Context context, int userId, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<Integer> arrayList3, ArrayList<String> arrayList4, ArrayList<Integer> arrayList5, ArrayList<String> arrayList6, ArrayList<Integer> arrayList7, Timer timer){

        importantUserId = userId;
        mListItems = arrayList; // nome do quarto
        mListItems2 = arrayList2; // tipo de jogo
        mListItems3 = arrayList3; // imagem para entrar no quarto
        mListItems4 = arrayList4; // número de pessoas
        mListItems5 = arrayList5; // identificação do quarto (id)
        mListItems6 = arrayList6; // password do quarto
        mListItems7 = arrayList7; // dono do quarto
        roomsTimer = timer;
        ctx = context;

        //get the layout inflater
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        //getCount() represents how many items are in the list
        return mListItems.size();
    }

    @Override
    //get the data of an item from a specific position
    //i represents the position of the item in the list
    public Object getItem(int i) {
        return null;
    }

    @Override
    //get the position id of the item from the list
    public long getItemId(int i) {
        return 0;
    }


    public void insertPassword(final String passwordRoom, final int idRoom, final String gameType, final String numberPeople, final View v) {

        View view = (LayoutInflater.from(ctx)).inflate(R.layout.password_layout,null); // password_layout é o layout onde está os items que vai abrir na caixa de Diálogo
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ctx);
        alertBuilder.setView(view);

        final EditText password = (EditText) view.findViewById(R.id.roompasswordinput); // Buscar a password que o utilizador escreveu


        // o que acontece ao clicar no botão Ok
        alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (password.getText().toString().equals(passwordRoom)) { // Se a password estiver correta
                    // Password correta
                    verifyRoom(gameType, numberPeople, idRoom, v); // Se password estiver correta, verifica se são equipas
                } else {
                    // Password errada
                    Toast.makeText(ctx.getApplicationContext(), ctx.getApplicationContext().getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();
                    v.setEnabled(true);
                }

            }
        });

        // O que acontece ao clicar no botão Cancel
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        Dialog dialog = alertBuilder.create();
        dialog.show(); // mostrar a caixa e diálogo

    }


    public void chooseTeam(final String playersRoom, final int idRoom, final View v) {

        View view = (LayoutInflater.from(ctx)).inflate(R.layout.team_layout,null); // team_layout é o layout onde está os items que vai abrir na caixa de Diálogo
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ctx);
        alertBuilder.setView(view);

        final RadioGroup group = (RadioGroup) view.findViewById(R.id.team); // Radio group para escolher a equipa


        // Colocar a equipa A já pré definido
        final RadioButton radio_teama = (RadioButton) view.findViewById(R.id.radio_teama);
        final RadioButton radio_teamb = (RadioButton) view.findViewById(R.id.radio_teamb);

        radio_teama.setChecked(true);
        teamChoose = 1;


        // Ao selecionar uma opção do radiogroup
        // Radiogroup permite que apenas 1 das opções dos radiobuttons sejam selecionadas
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (radio_teama .isChecked() == true) { // Escolheu Equipa A (resto da divisão 1) -> impares
                    teamChoose = 1;
                } else if (radio_teamb.isChecked() == true)  { // Escolheu Equipa B (resto da divisão 0) -> pares
                    teamChoose = 2;
                }


            }
        });


        // o que acontece ao clicar no botão Join
        alertBuilder.setPositiveButton("Join", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String[] players = playersRoom.split(" - "); // Vai cortar o nº de jogadores ao encontrar " - ", para distinguir os jogadores da equipa A e os da B
                String teamA = players[0]; // Jogadores equipa A (atuais e máximo)
                String teamB = players[1]; // Jogadores equipa B (atuais e máximo)

                String[] tA = teamA.split("/"); // Equipa A - Vai cortar ao encontrar "/", logo vai separar os jogadores atuais do máximo (ex. 5/10 separa 5 do 10)
                String[] tB = teamB.split("/"); // Equipa B - Vai cortar ao encontrar "/", logo vai separar os jogadores atuais do máximo

                int roomPlayersA = Integer.parseInt(tA[0]); // Nº jogadores na equipa A
                int maxPlayersA = Integer.parseInt(tA[1]); // Nº máximo jogadores equipa A
                int roomPlayersB = Integer.parseInt(tB[0]); // Nº jogadores na equipa B
                int maxPlayersB = Integer.parseInt(tB[1]); // Nº máximo jogadores equipa B


                int rp=0, mp=0; // room players e max players

                if (teamChoose == 1) { // Equipa A

                    rp = roomPlayersA;
                    mp = maxPlayersA;

                } else if (teamChoose == 2) { // Equipa B

                    rp = roomPlayersB;
                    mp = maxPlayersB;

                }


                if (rp+1 <= mp) { // Se o nº de jogadores atual + 1 não superar o máximo, pode entrar

                    roomsTimer.cancel(); // Pára de atualizar os quartos

                    userEntersOnRoom(idRoom, importantUserId, 1, teamChoose); // Adiciona o utilizador ao quarto na base de dados

                    Intent i = new Intent(ctx.getApplicationContext(), GameWaiting.class);
                    // nome da variável enviada no 1º argumento (utilizada para receber na outra atividade) e o valor é o 2º argumento
                    i.putExtra("roomId", idRoom); // envia o id do quarto para a nova atividade (GameWaiting)
                    i.putExtra("importantUserId", importantUserId);
                    i.putExtra("teams", 1);
                    i.putExtra("numPlayersA", "" + (roomPlayersA+1)); // envia o nº de jogadores no quarto, da equipa A
                    i.putExtra("numPlayersB", "" + (roomPlayersB+1)); // Envia o nº de jogadores no quarto, da equipa B
                    i.putExtra("maxPlayers", "" + maxPlayersA); // maxPlayersA é igual ao maxPlayersB, é o máx permitido
                    ctx.startActivity(i);
                } else { // Equipa cheia
                    Toast.makeText(ctx.getApplicationContext(), ctx.getApplicationContext().getString(R.string.team_full), Toast.LENGTH_SHORT).show();
                    v.setEnabled(false);
                }

            }
        });

        // O que acontece ao clicar no botão Cancel
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        Dialog dialog = alertBuilder.create();
        dialog.show(); // mostrar a caixa e diálogo

    }



    // Este método verifica o tipo de jogo e verifica se há espaço disponivel
    public void verifyRoom(String gameType, String numberPeople, int idRoom, View v) {
        if (gameType.equals("Individually")) { // Se for um jogo individual

            String[] players = numberPeople.split("/"); // Vai cortar o nº de jogadores ao encontrar "/", para distinguir os que estão do máximo (ex. 5/10 separa 5 do 10)
            int roomPlayers = Integer.parseInt(players[0]); // Nº de jogadores que estão atualmente no quarto, convertendo para int
            int maxPlayers = Integer.parseInt(players[1]); // Nº máximo de jogadores que o quarto suporta, convertendo para int


            if (roomPlayers+1 <= maxPlayers) { // Se o nº de jogadores atual + 1 não superar o máximo, pode entrar

                roomsTimer.cancel(); // Pára de atualizar os quartos

                userEntersOnRoom(idRoom, importantUserId, 0, 0); // Adiciona o utilizador ao quarto na base de dados

                Intent i = new Intent(ctx.getApplicationContext(), GameWaiting.class);
                i.putExtra("roomId", idRoom); // envia o id do quarto para a nova atividade (GameWaiting)
                i.putExtra("importantUserId", importantUserId);
                i.putExtra("teams", 0); // 0 -> individual / 1 -> equipas
                i.putExtra("numPlayersA", "" + (roomPlayers+1)); // Envia o nº de jogadores no quarto
                i.putExtra("numPlayersB", "0"); // Não é utilizado pois é jogo individual
                i.putExtra("maxPlayers", "" + maxPlayers); // Máximo de jogares permitido no quarto
                ctx.startActivity(i);
            } else { // Quarto cheio
                Toast.makeText(ctx.getApplicationContext(), ctx.getApplicationContext().getString(R.string.room_full), Toast.LENGTH_SHORT).show();
                v.setEnabled(true);
            }

        } else if (gameType.equals("Teams")) { // Se for um jogo em equipas

            chooseTeam(numberPeople, idRoom, v);

        }

    }



    // Este método adiciona um utilizador a um quarto, na base de dados
    public void userEntersOnRoom(int id_room, int id_user, int teams, int team) {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("id_room", "" + id_room));
        nameValuePairs.add(new BasicNameValuePair("id_user", "" + id_user));
        nameValuePairs.add(new BasicNameValuePair("teams", "" + teams));
        nameValuePairs.add(new BasicNameValuePair("team", "" + team));

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/enter_room.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(ctx, "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8); // iso-8859-1
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

            /* Retorno dos dados */
        try {
            JSONObject json_data = new JSONObject(result);
            success = (json_data.getInt("success")); /* Vai buscar ao json o campo "name" */

            if (success == 0) { // Erro
                Toast.makeText(ctx, ctx.getString(R.string.error), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
            }

        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

    }



    @Override

    public View getView(final int position, View view, ViewGroup viewGroup) {

        // create a ViewHolder reference
        ViewHolder holder;

        //check to see if the reused view is null or not, if is not null then reuse it
        if (view == null) {
            holder = new ViewHolder();

            // Id e password não ficam aqui, porque apenas é guardado, não é mostrado
            view = mLayoutInflater.inflate(R.layout.lobby_item, null); // lobby_item é o layout em que estão os items pertencentes à ListView (ou seja, a estrutura de cada linha)
            holder.itemRoomName = (TextView) view.findViewById(R.id.room_name); // Buscar o id da TextView onde vai colocar os valores da ArrayList.
            holder.itemGameType = (TextView) view.findViewById(R.id.game_type); // Buscar o id da TextView onde vai colocar os valores da ArrayList.
            holder.itemEnterRoom = (ImageView) view.findViewById(R.id.enter); // Buscar o id da ImageView onde tem a imagem para entrar no quarto
            holder.itemNumberPeople = (TextView) view.findViewById(R.id.number_people); // Buscar o id da TextView onde vai colocar os valores da ArrayList.

            // the setTag is used to store the data within this view
            view.setTag(holder);
        } else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)view.getTag();
        }

        //get the string item from the position "position" from array list to put it on the TextView
        String stringItem = mListItems.get(position); // A variável stringItem fica com o nome do quarto do item da ArrayList.
        final String stringItem2 = mListItems2.get(position);  // A variável stringItem2 fica com o tipo de jogo do item da ArrayList.
        Integer stringItem3 = mListItems3.get(position); // A variável stringItem3 fica com a imagem de entrar do item da ArrayList.
        // final porque é usado aqui dentro
        final String stringItem4 = mListItems4.get(position); // A variável stringItem4 fica com o número de pessoas do item da ArrayList.
        final Integer stringItem5 = mListItems5.get(position); // A variável stringItem5 fica com o id do quarto do item da ArrayList.
        final String stringItem6 = mListItems6.get(position); // A variável stringItem6 fica com a password do quarto do item da ArrayList.
        final Integer stringItem7 = mListItems7.get(position); // A variável stringItem7 fica com o dono do quarto do item da ArrayList.

        // Ao clicar na linha da listview
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view.setEnabled(false);

                if (!stringItem6.equals("")) { // Caso o quarto tenha password
                    insertPassword(stringItem6, stringItem5, stringItem2, stringItem4, view);
                } else {
                    verifyRoom(stringItem2, stringItem4, stringItem5, view); // Verifica se há espaço disponível
                }

            }
        });

        // A variável imageView fica com a ImageView cujo id é enter.
        /* final ImageView imageView = (ImageView) view.findViewById(R.id.enter);
        imageView.setTag(new Integer(position));
        imageView.setOnClickListener(new View.OnClickListener() { // Ação ao clicar na imagem da ListView (ao clicar para entrar no quarto)

            @Override
            public void onClick(View view) {

                imageView.setEnabled(false);

                if (!stringItem6.equals("")) { // Caso o quarto tenha password
                    insertPassword(stringItem6, stringItem5, stringItem2, stringItem4, imageView);
                } else {
                    verifyRoom(stringItem2, stringItem4, stringItem5, imageView); // Verifica se há espaço disponível
                }

            }

        }); */

        if (stringItem7 == importantUserId) { // Se o quarto pertencer ao user que fez login, destaca a verde
            holder.itemRoomName.setTextColor(Color.GREEN);
        } else {
            holder.itemRoomName.setTextColor(Color.WHITE);
        }


        if (stringItem != null) { // verifica se não está vazio o campo room name da ArrayList stringItem
            if (holder.itemRoomName != null) {
                //set the item name on the TextView
                holder.itemRoomName.setText(stringItem);
            }
        }
        if (stringItem2 != null) { // verifica se não está vazio o campo game type da ArrayList stringItem2
            if (holder.itemGameType != null) {
                //set the item name on the TextView
                holder.itemGameType.setText(stringItem2);
            }
        }

        if (stringItem3 != null) { // verifica se não está vazio o campo enterRoom da ArrayList stringItem3
            if (holder.itemEnterRoom != null) {
                //set the item name on the TextView
                holder.itemEnterRoom.setImageResource(stringItem3);
            }
        }

        if (stringItem4 != null) { // verifica se não está vazio o campo number of people da ArrayList stringItem4
            if (holder.itemNumberPeople != null) {
                //set the item name on the TextView
                holder.itemNumberPeople.setText(stringItem4);
            }
        }

        //this method must return the view corresponding to the data at the specified position.
        return view;

    }

    /**
     * Used to avoid calling of "findViewById" every time the getView() method is called,
     * because this can impact to your application performance when your list is large
     */
    private class ViewHolder {

        protected TextView itemRoomName;
        protected TextView itemGameType;
        protected TextView itemNumberPeople;
        protected ImageView itemEnterRoom;
    }

}