package nuno_velosa.group9_pms;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;



public class Countdown extends AppCompatActivity {

    // Variáveis para os intents (variáveis recebidas de outra atividade)
    Serializable valor;
    Serializable playingInTeams;
    Serializable id;

    int roomId, teams;


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    TextView counter;
    int cont = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.countdown);


        // Buscar a Intent
        Intent intent = getIntent();

        // Buscar a variável
        // Irá guardar na variável valor, a variável que foi enviada com a identificação "roomId"
        valor = intent.getSerializableExtra("roomId");
        roomId = (Integer) valor;
        // Vai buscar id do utilizador que fez login
        id = intent.getSerializableExtra("importantUserId");
        importantUserId = (Integer) id;
        playingInTeams = intent.getSerializableExtra("teams");
        teams = (Integer) playingInTeams;

        counter = (TextView) findViewById(R.id.counter);

        // Conta durante 5 segundos (5000) de 1 em 1 segundo (1000)
        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                // Ação enquanto está a contar
                cont--;
                counter.setText("" + cont);
            }
            public void onFinish() {
                // Ação ao finalizar a contagem (os 3 segundos) -> mudar de página

                Intent game = new Intent(getApplicationContext(), Game.class);
                game.putExtra("importantUserId", importantUserId); // Enviar id do utilizador para a atividade seguinte
                game.putExtra("roomId", (Integer) valor);
                game.putExtra("teams", teams); // Envia o tipo de jogo
                startActivity(game);

            }
        }.start();


        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Colocar a app fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


    }



    /* Retirar ação do botão de voltar para trás */
    @Override
    public void onBackPressed() {

    }


}
