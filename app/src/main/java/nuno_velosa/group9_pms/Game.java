package nuno_velosa.group9_pms;

// Fazer os imports

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.EstimoteSDK;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.estimote.sdk.cloud.EstimoteCloud;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;



// Sqlite
import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;


public class Game extends Activity {

    // Para ir buscar imagens através do link
    ImageLoader imgLoader;

    GridView game_beacons_list;
    private GridViewAdapter mAdapter; // adaptador para a gridview (para preencher)
    ListView game_players_scores;
    private RoomAdapter rAdapter; // adaptador para a listview (para preencher)
    /* Arraylists para os beacons */
    private ArrayList<String> beaconsImage;
    private ArrayList<String> beaconsName;
    private ArrayList<Integer> beaconsId;
    private ArrayList<Integer> beaconsUser;
    private ArrayList<String> beaconsImageFound;
    /* Arraylists para os jogadores */
    private ArrayList<Integer> playersOfTheRoom;
    private ArrayList<String> playerScore;
    private ArrayList<String> playerImage;
    private ArrayList<Integer> playerId;
    private ArrayList<String> beaconsFound;
    private ArrayList<String> beaconsRoom;


    // Estimote
    private BeaconManager beaconManager;
    private EstimoteCloud estimoteCloud;
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);


    // Sqlite
    SQLiteDatabase bancoDados = null;
    Cursor cursor;



    TextView score1, score2;
    ImageView img1, img2;


    // Variáveis para aceder à base de dados
    InputStream is=null;
    String result=null;
    String line=null;
    String inserted;
    int success;
    JSONArray beaconsList = null, playersList = null;


    // Variáveis para os intents (variáveis recebidas de outra atividade)
    Serializable valor;
    Serializable playingInTeams;


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    Timer timer;


    private ArrayList<Integer> colors; // todas as cores (20 porque é o máximo de jogadores no quarto)



    // Método onCreate() é chamado ao iniciar a aplicação.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.game);


        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Colocar a app fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Buscar a Intent
        Intent intent = getIntent();

        // Buscar a variável
        // Irá guardar na variável valor, a variável que foi enviada com a identificação "roomId"
        valor = intent.getSerializableExtra("roomId");
        // Vai buscar id do utilizador que fez login
        Serializable id = intent.getSerializableExtra("importantUserId");
        importantUserId = (Integer) id;
        playingInTeams = intent.getSerializableExtra("teams");
        final int teams = (Integer) playingInTeams;


        // Adiciona 20 cores ao array
        colors = new ArrayList<Integer>();
        fillColors();



        score1 = (TextView) findViewById(R.id.player_name);
        img1 = (ImageView) findViewById(R.id.player_image);
        img2 = (ImageView) findViewById(R.id.player_image_teamb);
        score2 = (TextView) findViewById(R.id.player_name_teamb);


        if (teams == 0) { // Se for individual, retira uma das caixas do score
            img2.setVisibility(View.GONE);
            score2.setVisibility(View.GONE);
        } else {
            img2.setVisibility(View.VISIBLE);
            score2.setVisibility(View.VISIBLE);
        }


        imgLoader = new ImageLoader(getApplicationContext());



        game_beacons_list = (GridView) findViewById(R.id.beacons_list); // Lista dos beacons
        game_players_scores = (ListView) findViewById(R.id.players_list); // Lista dos jogadores
        beaconsImage = new ArrayList<String>();
        beaconsName = new ArrayList<String>();
        beaconsId = new ArrayList<Integer>();
        beaconsUser = new ArrayList<Integer>();
        beaconsImageFound = new ArrayList<String>();
        playersOfTheRoom = new ArrayList<Integer>();
        playerScore = new ArrayList<String>();
        playerId = new ArrayList<Integer>();
        playerImage = new ArrayList<String>();

        beaconsFound = new ArrayList<String>();
        beaconsRoom = new ArrayList<String>();



        // Cria a base de dados sqlite, caso não exista
        abreouCriaBanco(); // Cria a base de dados caso não exista, ou abre caso exista


        // apagar(); // Apaga todos os beacons do SQLITE
        //gravar(1, "Seabird", 55208, 65182, "icy_marshmallow");
        // gravar(2, "Gournard", 62739, 10230, "mint_cocktail");


        // Atribuir cores ao utilizador atual (cuja caixa do score está em baixo)
        getPlayersBeacons(teams, true); // Este true significa que vai adicionar os beacons do quarto ao sqlite
        addColorToUser();



        /* if (buscarDados(7)) {
            mostrarDados();
        } else {
            Toast.makeText(getBaseContext(), "No information", Toast.LENGTH_SHORT);
        } */



        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                getPlayersBeacons(teams, false); // Atualiza o score dos jogadores e vai buscar os beacons do jogo, a cada 2 segundos
            }
        };

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(runnable);

            }
        }, 0, 4000);   // 4000 milisegundos = 4 segundos



        // Estimote (conectar com a nossa app)
        beaconManager = new BeaconManager(this);
        EstimoteSDK.initialize(this.getApplicationContext(), "beacon-boys-d76", "a57a0571de9a0dbd064f4076b28bd5c1");
        //Optional, debug logging.
        EstimoteSDK.enableDebugLogging(true);


        SystemRequirementsChecker.checkWithDefaultDialogs(this); // to enable bluetooth

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {

            @Override
            public void onServiceReady() {

                beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
            }

        });


         /* Ao encontrar beacons */
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {

            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {

                // Verificar se o jogo já terminou (quando todos os beacons forem encontrados)
                // Quando nº beacons encontrados for igual ao nº de beacons do quarto, significa que todos foram encontrados

                if (beacons.size() != 0) {

                    for (int i = 0; i < beacons.size(); i++) {


                        // Verificar se é um dos beacons associados ao quarto
                        // Cada beacon associado ao quarto é guardado do tipo minormajor (ex. 2000355208)
                        if (beaconsRoom.contains(beacons.get(i).getMinor() + "" + beacons.get(i).getMajor())) {
                            // Beacon está associado ao quarto
                            // Verificar se já foi encontrado
                            if (!beaconsFound.contains(beacons.get(i).getMinor() + "" + beacons.get(i).getMajor())) {
                                // Beacon não foi encontrado ainda, logo BEACON ENCONTRADO

                                Toast.makeText(getBaseContext(), getString(R.string.beacon_found), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                                beaconsFound.add(beacons.get(i).getMinor() + "" + beacons.get(i).getMajor());
                                // Toca um som ao encontrar beacon
                                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.beacon_found);
                                mp.start();

                                // Mostra popup ao encontrar beacon
                                if (buscarDados(beacons.get(i).getMajor(), beacons.get(i).getMinor())) {
                                    mostrarDados();
                                } else {
                                    Toast.makeText(getBaseContext(), "No information", Toast.LENGTH_SHORT);
                                }

                                // Guarda na base de dados que foi encontrado
                                beaconFound(beacons.get(i));

                            }

                        }


                    }

                }

            }

        });



    }



    /* Este método  cria a base de dados se ainda não existir, e abre-a */
    public void abreouCriaBanco() {
        try {
            String nomeBanco = "room_beacons";
            // cria ou abre a base de dados
            bancoDados = openOrCreateDatabase(nomeBanco, MODE_PRIVATE, null);
            String sql = "CREATE TABLE IF NOT EXISTS room_beacons " + "(id_beacon INTEGER, name TEXT, major INTEGER, minor INTEGER, image TEXT);";
            bancoDados.execSQL(sql);
            // alerta("Sucesso","Base de dados criada com sucesso","Ok");
        } catch (Exception erro) {
            // alerta("Erro","Erro ao abrir ou criar a base de dados" + erro.getMessage(),"Ok");
        }
    }



    /* Este método permite apagar todos os beacons, para depois ser inserido novamente, pois esta tabela, só guarda os beacons do quarto atual */
    public void apagar() {
        try {
            String sql = "DELETE from room_beacons";
            bancoDados.execSQL(sql);
            // alerta("Sucesso", "Dados gravados com sucesso", "Ok");
        } catch (Exception erro) {
            // alerta("Erro","Não foi possível guardar os dados" + erro.getMessage(),"Ok");
        }
    }



    /* Este método permite gravar os beacons no sqlite */
    public void gravar (int id_beacon, String name, int major, int minor, String image) {

        try {
            String sql = "INSERT into room_beacons (id_beacon, name, major, minor, image) values (" + id_beacon + ",'" + name + "'," + major + "," + minor + ",'" + image + "')";
            bancoDados.execSQL(sql);
            // alerta("Sucesso", "Dados gravados com sucesso", "Ok");
        } catch (Exception erro) {
            // alerta("Erro","Não foi possível guardar os dados" + erro.getMessage(),"Ok");
        }

    }



    /* Este método vai buscar a informação de um determinado beacon */
    private boolean buscarDados(int major, int minor) {
        try {

            cursor = bancoDados.rawQuery("Select * from room_beacons where major=" + major + " and minor=" + minor,null);

            // contar número de registos

            int numero_registos = cursor.getCount();
            if (numero_registos != 0) { // se houver registos
                cursor.moveToFirst(); // posiciona no 1º registo

                return true;
            } else {
                return false;

            }

        } catch (Exception erro) {
            // alerta("Erro","Erro ao buscar dados" + erro.getMessage(),"Ok");
            return false;
        }
    }



    /* Este método permite mostrar os dados dos beacons na popup (vindo do SQLITE) */
    public void mostrarDados() {

        int Posid = cursor.getColumnIndex("id_beacon"); // Devolve o index (int)
        int Posname = cursor.getColumnIndex("name");
        int Posmajor = cursor.getColumnIndex("major");
        int Posminor = cursor.getColumnIndex("minor");
        int Posimage = cursor.getColumnIndex("image");

        popupBeacon(cursor.getString(Posname), cursor.getString(Posimage));

        // mostrar dados
        Log.e("BUSCA", cursor.getInt(Posid) + " " + cursor.getString(Posname) + " " + cursor.getInt(Posmajor) + " " + cursor.getInt(Posminor) + " " + cursor.getString(Posimage));

    }



    /* Este método permite fechar a base de dados. */
    public void fechaBanco() {
        try {
            bancoDados.close(); // fecha base de dados
        } catch (Exception erro) {
            // alerta("Erro","Erro ao fechar a base de dados" + erro.getMessage(),"Ok");
        }
    }




    /* Este método permite atribuir uma cor ao utilizador que está na conta, para saber quais beacons encontrou */
    public void addColorToUser() {


        // Adicionar cor à border do utilizador (para depois sabre quem apanhou o beacon)
        int color = 0, pos = 0;


        if (playersOfTheRoom.contains(importantUserId)) {
            pos = playersOfTheRoom.indexOf(importantUserId);
            color = colors.get(pos);
        }


        //use a GradientDrawable with only one color set, to make it a solid color
        GradientDrawable border = new GradientDrawable();
        border.setColor(color); //white background
        // border.setStroke(1, color); //black border with full opacity
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            score1.setBackgroundDrawable(border);
        } else {
            score1.setBackground(border);
        }


    }




    public void unassignBeacons_updateScores() {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.
        nameValuePairs.add(new BasicNameValuePair("roomId", "" + (Integer) valor)); // Envia para o ficheiro php

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/game_ends.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8); // Foi mudado para UTF-8 por causa de um erro que acontecia no JSON
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {

            JSONObject json_data = new JSONObject(result);
            inserted = (json_data.getString("success"));


        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }
    }




    /* Popup que irá aparecer cada vez que um jogador encontrar um beacon */
    public void popupBeacon(String beaconName, String beaconImage) {

        // Criar layout da modal
        View view = (LayoutInflater.from(Game.this)).inflate(R.layout.found_beacon ,null); // found_beacon é o layout onde está os items que vai abrir na caixa de Diálogo
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Game.this);
        alertBuilder.setView(view);

        ImageView imageBeacon = (ImageView) view.findViewById(R.id.imagebeacon);
        TextView namebeacon = (TextView) view.findViewById(R.id.namebeacon);

        // Alterar imagem do beacon no popup
        int image = getApplicationContext().getResources().getIdentifier(beaconImage + "_big","drawable", getApplicationContext().getPackageName());
        imageBeacon.setImageResource(image);

        // Alterar nome do beacon no popup
        namebeacon.setText(beaconName);

        final Dialog dialog = alertBuilder.create();
        dialog.show(); // mostrar a caixa e diálogo

        /* alertBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        }); */


        // Passados 10 segundos, o popup é fechado automaticamente
        /* new CountDownTimer(10000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                // TODO Auto-generated method stub

            }
            @Override
            public void onFinish() {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        }.start(); */

    }




    /* Este método é chamado quando um beacon do jogo é encontrado, da seguinte forma:
    - Coloca na base de dados, o beacon como encontrado e guarda o jogador que o apanhou
    - Aumenta 10 pontos no score do jogador que encontrou o beacon
     */
    public void beaconFound(Beacon b) {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.
        nameValuePairs.add(new BasicNameValuePair("roomId", "" + (Integer) valor)); // Envia para o ficheiro php
        nameValuePairs.add(new BasicNameValuePair("userId", "" + importantUserId));
        nameValuePairs.add(new BasicNameValuePair("minor", "" + b.getMinor()));
        nameValuePairs.add(new BasicNameValuePair("major", "" + b.getMajor()));

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/beacon_found.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8); // Foi mudado para UTF-8 por causa de um erro que acontecia no JSON
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {

            JSONObject json_data = new JSONObject(result);
            inserted = (json_data.getString("success"));
            // String beaconName = (json_data.getString("beaconName"));
            // String beaconImage = (json_data.getString("beaconImage"));

            if (inserted.equals("0")) {
                Toast.makeText(getBaseContext(), getString(R.string.already_found), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
            }
            /* else if (inserted.equals("1")) {
                popupBeacon(beaconName, beaconImage);
            } */


        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }
    }



    /* Este método adiciona cores ao array, para atribuir aos jogadores */
    public void fillColors() {
        // últimos FF são a transparência (0 transparência)
        colors.add(0x0000FFFF); // 1
        colors.add(0xffa366FF); // 2
        colors.add(0xd9b3ffFF); // 3
        colors.add(0x00FF00FF); // 4
        colors.add(0xFFFF00FF); // 5
        colors.add(0x000000FF); // 6
        colors.add(0xD16285FF); // 7
        colors.add(0x00FFFFFF); // 8
        colors.add(0xFFB500FF); // 9
        colors.add(0xFCB5FFFF); // 10
        colors.add(0x54B5FFFF); // 11
        colors.add(0x38F29CFF); // 12
        colors.add(0x744A9CFF); // 13
        colors.add(0x552A1AFF); // 14
        colors.add(0xFF6300FF); // 15
        colors.add(0x030144FF); // 16
        colors.add(0xCDD844FF); // 17
        colors.add(0x39735CFF); // 18
        colors.add(0x006633FF); // 19
        colors.add(0x606060FF); // 20
    }




    /* Este método vai buscar os jogadores deste jogo */
    public void getPlayersBeacons(int teams, boolean addToSqlite) {

        // Limpa os arrays
        playersOfTheRoom.clear();
        playerScore.clear();
        playerId.clear();
        playerImage.clear();

        beaconsName.clear();
        beaconsImage.clear();
        beaconsId.clear();
        beaconsUser.clear();
        beaconsImageFound.clear();

        beaconsRoom.clear();
        beaconsFound.clear();

        // Limpa o adaptador
        game_players_scores.setAdapter(null);

        game_beacons_list.setAdapter(null);


        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.
        nameValuePairs.add(new BasicNameValuePair("roomId", "" + (Integer) valor)); // Envia para o ficheiro php
        nameValuePairs.add(new BasicNameValuePair("teams", "" + teams));

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/gamePlayers.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success11  ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8); // Foi mudado para UTF-8 por causa de um erro que acontecia no JSON
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success22 "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {

            JSONObject json_data = new JSONObject(result);


            // Buscar jogadores do jogo
            playersList = json_data.getJSONArray("list"); // Buscar array rooms

            for (int i = 0; i < playersList.length(); i++) { // Percorre a lista de jogadores
                JSONObject r = playersList.getJSONObject(i); // Vai buscar o elemento json atual (jogador atual)

                playersOfTheRoom.add(r.getInt("id_user"));


                if (r.getInt("id_user") == importantUserId && teams == 0) { // Se for o próprio, num jogo invidual, mostra o score em baixo

                    score1.setText("" + r.getInt("score"));
                    imgLoader.DisplayImage(json_data.getString("image"), R.drawable.loader, img1);

                } else { // Se não for o próprio ou estiver em jogos de equipas, mostra ao lado
                    playerScore.add("" + r.getInt("score"));
                    playerImage.add(r.getString("image"));
                    playerId.add(r.getInt("id_user"));
                }



                if (teams == 1) { // Atualiza scores das equipas
                    score1.setText("" + json_data.getInt("scoreA"));
                    score2.setText("" + json_data.getInt("scoreB"));
                }

            }



            // Buscar beacons do jogo
            beaconsList = json_data.getJSONArray("beacons"); // Buscar array dos beacons

            for (int i = 0; i < beaconsList.length(); i++) { // Percorre a lista de beacons
                JSONObject r = beaconsList.getJSONObject(i); // Vai buscar o elemento json atual (beacon atual)


                if (addToSqlite) { // Se for para adicionar ao SQLITE os beacons do quarto (1º vez apenas)

                    gravar(r.getInt("id_beacon"), r.getString("name"), Integer.parseInt(r.getString("major")), Integer.parseInt(r.getString("minor")), r.getString("image"));

                }


                beaconsName.add(r.getString("name"));
                beaconsId.add(r.getInt("id_beacon"));
                if (r.getInt("found") == 0) {

                    beaconsImage.add(r.getString("image")); // Se não for encontrado, fica imagem normal

                } else if (r.getInt("found") == 1) {

                    beaconsImage.add(r.getString("image_found")); // Se beacon for encontrado, imagem com certo em cima
                    beaconsFound.add(r.getString("minor") + "" + r.getString("major"));
                }

                beaconsUser.add(r.getInt("id_user"));

                // beaconsImageFound.add(r.getString("image_found"));
                // Guarda neste array os beacons, para posteriormente ser feita a comparação
                beaconsRoom.add(r.getString("minor") + "" + r.getString("major"));

            }


            if (beaconsRoom.size() == beaconsFound.size()) { // Todos os beacons encontrados

                // Jogo terminou
                timer.cancel(); // Pára de atualizar os scores

                // Cancela a procura de beacons
                beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);


                // Desassocia os beacons
                // Atualiza os scores dos jogadores na base de dados (pontos pessoais)
                unassignBeacons_updateScores();


                // Envia o id do quarto para nova atividade (onde mostrará o vencedor)
                Intent i = new Intent(getApplicationContext(), Winner.class);
                i.putExtra("roomId", (Integer) valor);
                i.putExtra("importantUserId", importantUserId);
                i.putExtra("teams", teams);
                startActivity(i);

            }


        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }


        game_players_scores.setAdapter(new RoomAdapter(Game.this, importantUserId, playerScore, playerId, playerImage, 2, playerId));
        game_players_scores.setTextFilterEnabled(true);


        mAdapter = new GridViewAdapter(getBaseContext(), this, beaconsImage, beaconsName, beaconsUser, playersOfTheRoom, colors);
        game_beacons_list.setAdapter(mAdapter);


    }





    /* @Override
    protected void onResume() {

        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this); // to enable bluetooth

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {

            @Override
            public void onServiceReady() {

                beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);        }

        });

    } */



    /* Retirar ação do botão de voltar para trás */
    @Override
    public void onBackPressed() {

    }


    @Override
    public void onStop() {
        fechaBanco(); // Fecha a base de dados
        super.onStop();
    }



}
