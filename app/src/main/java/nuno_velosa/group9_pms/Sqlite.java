package nuno_velosa.group9_pms;

// Fazer os imports

import android.app.Activity;
import android.os.Bundle;


import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;


public class Sqlite extends Activity {

    SQLiteDatabase bancoDados = null;
    Cursor cursor;

    // Método onCreate() é chamado ao iniciar a aplicação.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        abreouCriaBanco(); // Cria a base de dados caso não exista, ou abre caso exista

        apagar(); // Apaga todos os registos
        gravar(1, "Seabird", 55208, 65182, "icy_marshmallow");
        gravar(2, "Gournard", 62739, 10230, "mint_cocktail");

        if (buscarDados(2)) {
            mostrarDados();
        } else {
            Toast.makeText(getBaseContext(), "No information", Toast.LENGTH_SHORT);
        }

        fechaBanco(); // Fecha a base de dados

    }



    private boolean buscarDados(int id) {
        try {

            cursor = bancoDados.rawQuery("Select * from beacons where id_beacon=" + id,null);

            // contar número de registos

            int numero_registos = cursor.getCount();
            if (numero_registos != 0) { // se houver registos
                cursor.moveToFirst(); // posiciona no 1º registo

                return true;
            } else {
                return false;
            }

        } catch (Exception erro) {
            // alerta("Erro","Erro ao buscar dados" + erro.getMessage(),"Ok");
            return false;
        }
    }




    public void mostrarDados() {

        int Posid = cursor.getColumnIndex("id_beacon"); // Devolve o index (int)
        int Posname = cursor.getColumnIndex("name");
        int Posmajor = cursor.getColumnIndex("major");
        int Posminor = cursor.getColumnIndex("minor");
        int Posimage = cursor.getColumnIndex("image");

        // mostrar dados
        Log.e("BUSCA", cursor.getInt(Posid) + " " + cursor.getString(Posname) + " " + cursor.getInt(Posmajor) + " " + cursor.getInt(Posminor) + " " + cursor.getString(Posimage));



    }



    public void apagar() {
        try {
            String sql = "DELETE from beacon";
            bancoDados.execSQL(sql);
            // alerta("Sucesso", "Dados gravados com sucesso", "Ok");
        } catch (Exception erro) {
            // alerta("Erro","Não foi possível guardar os dados" + erro.getMessage(),"Ok");
        }
    }



    public void gravar (int id_beacon, String name, int major, int minor, String image) {

        try {
            String sql = "INSERT into beacons (id_beacon, name, major, minor, image) values (" + id_beacon + ",'" + name + "'," + major + "," + minor + ",'" + image + "')";
            bancoDados.execSQL(sql);
            // alerta("Sucesso", "Dados gravados com sucesso", "Ok");
        } catch (Exception erro) {
            // alerta("Erro","Não foi possível guardar os dados" + erro.getMessage(),"Ok");
        }

    }



    public void abreouCriaBanco() {
        try {
            String nomeBanco = "beacons";
            // cria ou abre a base de dados
            bancoDados = openOrCreateDatabase(nomeBanco, MODE_PRIVATE, null);
            String sql = "CREATE TABLE IF NOT EXISTS beacons " + "(id_beacon INTEGER PRIMARY KEY, name TEXT, major INTEGER, minor INTEGER, image TEXT);";
            bancoDados.execSQL(sql);
            // alerta("Sucesso","Base de dados criada com sucesso","Ok");
        } catch (Exception erro) {
            // alerta("Erro","Erro ao abrir ou criar a base de dados" + erro.getMessage(),"Ok");
        }
    }



    public void fechaBanco() {
        try {
            bancoDados.close(); // fecha base de dados
        } catch (Exception erro) {
            // alerta("Erro","Erro ao fechar a base de dados" + erro.getMessage(),"Ok");
        }
    }


}