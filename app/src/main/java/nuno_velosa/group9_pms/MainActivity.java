package nuno_velosa.group9_pms;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

// import com.google.api.client.http.HttpResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Método chamado ao iniciar a app */
        setContentView(R.layout.activity_main); /* Primeiro layout a ser chamado */


        // Começa a animação com o logo da primeira página
        startZoomInAnimation();


        // Conta durante 2 segundos (2000) de 1 em 1 segundo (1000)
        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
                // Ação enquanto está a contar
            }
            public void onFinish() {
                // Ação ao finalizar a contagem (os 3 segundos) -> mudar de página
                Intent login = new Intent(getApplicationContext(), Login.class);
                startActivity(login);
            }
        }.start();


        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Colocar a app fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


    }



    /* Animação para o logo começar a aumentar */
    public void startZoomInAnimation() {
        ImageView imageView = (ImageView) findViewById(R.id.mainlogo); // mainlogo é o id da imagem que quero que aconteça a animação
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in_animation); // zoom_in_animation é o ficheiro xml que contém a animação
        imageView.startAnimation(animation); // começa a animação
    }



    /* Retirar ação do botão de voltar para trás */
    @Override
    public void onBackPressed() {

    }


}
