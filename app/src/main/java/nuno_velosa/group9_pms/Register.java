package nuno_velosa.group9_pms;

// Fazer os imports

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.UUID;


public class Register extends Activity {


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    EditText username, password, name;
    TextView back;
    String susername, spassword, sname;
    Button registo;
    InputStream is=null;
    String result=null;
    String line=null;
    String val;




    // Upload Image
    //Declaring views
    private Button buttonChoose;
    //Image request code
    private int PICK_IMAGE_REQUEST = 1;
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;
    //Bitmap to get image from gallery
    private Bitmap bitmap;
    //Uri to store the image uri
    private Uri filePath;
    String upload_url = "http://mars.tigerwhale.com/upload.php";


    // Método onCreate() é chamado ao iniciar a aplicação.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Escolher o layout (ficheiro xml) que vai abrir ao iniciar aplicação, neste caso activity_main.xml.
        setContentView(R.layout.register);

        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        registo = (Button) findViewById(R.id.continua);
        username = (EditText) findViewById(R.id.username_register);
        password = (EditText) findViewById(R.id.password_register);
        name = (EditText) findViewById(R.id.name_register);
        back = (TextView) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Login.class);
                startActivity(i);
            }
        });


        requestStoragePermission(); // Permitir permissão de acesso ao armazenamento do telemóvel
        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        uploadImage();


        registo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                registo.setEnabled(false);

                susername = username.getText().toString();
                spassword = password.getText().toString();
                sname = name.getText().toString();

                if (susername.equals("") || spassword.equals("") || sname.equals("")) { // Password ou username em branco
                    Toast.makeText(getBaseContext(), getString(R.string.fill_all), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                    registo.setEnabled(true); // Volta a colocar o botão pois podem voltar a clicar
                } else if (susername.length() < 6) {
                    Toast.makeText(getBaseContext(), getString(R.string.username_char), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                    registo.setEnabled(true);
                } else if (spassword.length() < 3) {
                    Toast.makeText(getBaseContext(), getString(R.string.password_char), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                    registo.setEnabled(true);
                } else if (sname.length() < 4) {
                    Toast.makeText(getBaseContext(), getString(R.string.name_char), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                    registo.setEnabled(true);
                } else {
                    register();
                }

            }
        });

    }



    /* Este método faz upload da imagem */
    public void uploadImage() {

        buttonChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

    }




    //Requesting permission
    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }




    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                // imageView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    // Este método permite escolher uma fotografia da galeria
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE_REQUEST);
    }






    /*
    * This is the method responsible for image upload
    * We need the full image path and the name for the image in this method
    * */
    public void uploadMultipart(int userId) {
        //getting name for the image
        String name = "image";

        //getting the actual path of the image
        String path = getPath(filePath);

        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();

            //Creating a multi part request
            new MultipartUploadRequest(this, uploadId, upload_url)
                    .addFileToUpload(path, "image") //Adding file
                    .addParameter("id", "" + userId) //Adding text parameter to the request
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

        } catch (Exception exc) {
            Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }




    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }



    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }




    /* Retirar ação do botão de voltar para trás */
    @Override
    public void onBackPressed() {

    }


    public void register() {


        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("username", susername)); // adiciona ao array no campo "username" a variável susername.
        nameValuePairs.add(new BasicNameValuePair("password", spassword)); // adiciona ao array no campo "password" a variável spassword.
        nameValuePairs.add(new BasicNameValuePair("name", sname)); // adiciona ao array no campo "name" a variável sname

        /* Conectar à página web e envio do array dos dados */
        try
        {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/register.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try
        {
            BufferedReader reader = new BufferedReader (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try
        {
            JSONObject json_data = new JSONObject(result);
            val = (json_data.getString("val")); /* Vai buscar ao json o campo "name" */
            importantUserId = (json_data.getInt("id_user")); // Vai buscar ao json o campo "id_user"

            if (val.equals("0")) { // Username já existe
                Toast.makeText(getBaseContext(), getString(R.string.already_exists), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                registo.setEnabled(false);
            } else {

                try {

                    // Faz upload da imagem, se tiver sido escolhida
                    uploadMultipart(importantUserId);

                } catch (NullPointerException e) {

                    // Não inseriu imagem, logo coloca uma predefinida
                    insertImage(importantUserId);

                }

                Intent i = new Intent(getApplicationContext(), Rooms.class);
                i.putExtra("importantUserId", importantUserId);
                startActivity(i);

                Toast.makeText(getBaseContext(), getString(R.string.welcome) + " " + sname + "!", Toast.LENGTH_SHORT).show(); // Dados errados
            }

        }
        catch(Exception e)
        {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }


    }



    /* Este método insere uma imagem ao utilizador, quando ele não insere nenhuma */
    public void insertImage(int userId) {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("userId", "" + userId));

        /* Conectar à página web e envio do array dos dados */
        try
        {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/insert_image.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try
        {
            BufferedReader reader = new BufferedReader (new InputStreamReader(is,"utf-8"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try
        {
            JSONObject json_data = new JSONObject(result);

        }
        catch(Exception e)
        {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

    }



}