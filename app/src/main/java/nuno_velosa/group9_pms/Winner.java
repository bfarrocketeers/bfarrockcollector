package nuno_velosa.group9_pms;

// Fazer os imports

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;


public class Winner extends Activity {


    ImageLoader imgLoader;


    // Variáveis para aceder à base de dados
    InputStream is=null;
    String result=null;
    String line=null;


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    Serializable valor, id, playingInTeams;


    // Método onCreate() é chamado ao iniciar a aplicação.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Colocar a app fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


        // Buscar a Intent
        Intent intent = getIntent();

        valor = intent.getSerializableExtra("roomId");
        id = intent.getSerializableExtra("importantUserId");
        playingInTeams = intent.getSerializableExtra("teams");
        importantUserId = (Integer) id;

        if ((Integer) playingInTeams == 0) { // Se for individual, mostra um layout
            setContentView(R.layout.winner_individual);
        } else if ((Integer) playingInTeams == 1) { // Se forem equipas. mostra outro layout
            setContentView(R.layout.winner_team);
        }


        imgLoader = new ImageLoader(getApplicationContext());



        getWinner();


        TextView exit_game = (TextView) findViewById(R.id.exit_game);

        exit_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Rooms.class);
                i.putExtra("importantUserId", importantUserId);
                startActivity(i);
            }
        });


    }




    // Buscar o vencedor do jogo
    // O vencedor do jogo é atualizado cada vez que encontra um beacon
    public void getWinner() {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.
        nameValuePairs.add(new BasicNameValuePair("roomId", (Integer) valor + "")); // Envia para o ficheiro php
        nameValuePairs.add(new BasicNameValuePair("teams", (Integer) playingInTeams + "")); // Envia para o ficheiro php
        nameValuePairs.add(new BasicNameValuePair("userId", importantUserId + "")); // Envia para o ficheiro php

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/getWinner.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {

            JSONObject json_data = new JSONObject(result);

            if (json_data.getInt("success") == 1) {

                if ((Integer) playingInTeams == 0) { // Individual

                    TextView txtt2 = (TextView) findViewById(R.id.txtt2);
                    txtt2.setText("" + json_data.getInt("scoreUser"));

                    ImageView img = (ImageView) findViewById(R.id.imgg);
                    imgLoader.DisplayImage(json_data.getString("image"), R.drawable.loader, img);

                    TextView txtt4 = (TextView) findViewById(R.id.txtt4);
                    txtt4.setText(json_data.getString("winner") + " - " + json_data.getInt("scoreWinner"));

                } else if ((Integer) playingInTeams == 1) { // Equipas

                    TextView txt2 = (TextView) findViewById(R.id.txt2);
                    txt2.setText("" + json_data.getString("nameA"));

                    TextView txt3 = (TextView) findViewById(R.id.txt3);
                    txt3.setText("" + json_data.getInt("scoreTeamA"));

                    TextView txt4 = (TextView) findViewById(R.id.txt4);
                    txt4.setText("" + json_data.getString("nameB"));

                    TextView txt5 = (TextView) findViewById(R.id.txt5);
                    txt5.setText("" + json_data.getInt("scoreTeamB"));

                }

            }


        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }


    }


}
