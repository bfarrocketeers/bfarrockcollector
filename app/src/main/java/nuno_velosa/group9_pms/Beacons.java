package nuno_velosa.group9_pms;

// Fazer os imports
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.EstimoteSDK;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.estimote.sdk.cloud.EstimoteCloud;



public class Beacons extends Activity {

    // Estimote
    private BeaconManager beaconManager;
    private EstimoteCloud estimoteCloud;
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);


    // Método onCreate() é chamado ao iniciar a aplicação.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Escolher o layout (ficheiro xml) que vai abrir ao iniciar aplicação, neste caso activity_main.xml.
        setContentView(R.layout.lobby);


        // Criação da listview
        final ListView listview = (ListView) findViewById(R.id.listview);
        final ArrayList<String> list = new ArrayList<String>();


        // Estimote (conectar com a nossa app)
        beaconManager = new BeaconManager(this);
        EstimoteSDK.initialize(this.getApplicationContext(), "beacon-boys-d76", "a57a0571de9a0dbd064f4076b28bd5c1");
        //Optional, debug logging.
        EstimoteSDK.enableDebugLogging(true);


        /* Ao encontrar beacons */
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {

            @Override
            public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {

                // Limpa a listview
                listview.setAdapter(null);

                // Limpa o array
                list.clear();

                if (beacons.size() != 0) {

                    for (int i = 0; i < beacons.size(); i++) {
                        Log.e("testee", "" + beacons.get(i));
                        if (beacons.get(i).getRssi() > -82) { // Diminuir distância dos beacons com a aplicação
                            list.add("" + beacons.get(i));
                        }
                    }




                }

                final StableArrayAdapter adapter = new StableArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, list);
                listview.setAdapter(adapter);


            }

        });

    }



    public double getDistance(int measuredPower, double rssi) {
        if (rssi >= 0) {
            return -1.0;
        }
        if (measuredPower == 0) {
            return -1.0;
        }
        double ratio = rssi * 1.0 / measuredPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            double distance= (0.42093) * Math.pow(ratio, 6.9476) + 0.54992;
            return distance;
        }
    }



    @Override
    protected void onResume() {

        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this); // to enable bluetooth

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {

            @Override
            public void onServiceReady() {

                beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);        }

        });

    }



        private class StableArrayAdapter extends ArrayAdapter<String> {

            HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

            public StableArrayAdapter(Context context, int textViewResourceId, List<String> objects) {
                super(context, textViewResourceId, objects);
                for (int i = 0; i < objects.size(); ++i) {
                    mIdMap.put(objects.get(i), i);
                }
            }

            @Override
            public long getItemId(int position) {
                String item = getItem(position);
                return mIdMap.get(item);
            }

            @Override
            public boolean hasStableIds() {
                return true;
            }

        }


}