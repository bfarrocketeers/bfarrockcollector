package nuno_velosa.group9_pms;

// Fazer os imports

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.Image;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.EstimoteSDK;
import com.estimote.sdk.Region;
import com.estimote.sdk.SystemRequirementsChecker;
import com.estimote.sdk.cloud.EstimoteCloud;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class Rooms extends Activity {


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    JSONArray listRooms = null;


    // Variáveis para aceder à base de dados
    InputStream is=null;
    String result=null;
    String line=null;
    int success;
    String name;
    int team;
    int idRoom;



    // Estimote
    private BeaconManager beaconManager;
    private EstimoteCloud estimoteCloud;
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid", null, null, null);



    ArrayList<String> roomNames = new ArrayList();
    ArrayList<String> gameTypes = new ArrayList();
    ArrayList<Integer> enterRoom = new ArrayList();
    ArrayList<String> numberPeople = new ArrayList();
    ArrayList<Integer> roomIds = new ArrayList(); // Guarda a identificação de cada quarto
    ArrayList<String> roomPasswords = new ArrayList();
    ArrayList<Integer> roomOwners = new ArrayList(); // Guarda o dono de cada quarto

    ArrayList<String> scores_list;

    Button newRoom, highscores, profile;

    Timer timer;

    JSONArray playersList = null;
    ListView top10_list;


    // Método onCreate() é chamado ao iniciar a aplicação.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Escolher o layout (ficheiro xml) que vai abrir ao iniciar aplicação, neste caso activity_main.xml.
        setContentView(R.layout.lobby);


        // Terminar sessão (logout)
        TextView logout = (TextView) findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Fazer logout do facebook caso tenha conta do facebook iniciada
                if (AccessToken.getCurrentAccessToken() == null) {
                    return; // Já fez logout
                }

                LoginManager.getInstance().logOut();


                Intent i = new Intent(getApplicationContext(), Login.class); // Ao fazer logout, volta para o login
                startActivity(i);


            }
        });


        newRoom = (Button) findViewById(R.id.newRoom);
        highscores = (Button) findViewById(R.id.top10);
        profile = (Button) findViewById(R.id.profile);

        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        // Buscar a Intent
        Intent intent = getIntent();

        // Buscar a variável
        // Irá guardar na variável valor, a variável que foi enviada com a identificação "roomId"
        Serializable valor = intent.getSerializableExtra("importantUserId");
        importantUserId = (Integer) valor;


        // Estimote (conectar com a nossa app)
        beaconManager = new BeaconManager(this);
        EstimoteSDK.initialize(this.getApplicationContext(), "beacon-boys-d76", "a57a0571de9a0dbd064f4076b28bd5c1");
        //Optional, debug logging.
        EstimoteSDK.enableDebugLogging(true);


        // Criação do timer para atualizar os quartos de 5 em 5 segundos
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                runOnUiThread(new Runnable() {
                    public void run() {
                        getRooms(); // Buscar jogadores do quarto
                    }
                });

            }
        }, 0, 5000);   // 5000 milisegundos = 5 segundos



        // Ao clicar no botão de criar nova sala
        newRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        // Ao clicar no botão de ver top 10
        scores_list = new ArrayList<String>();
        highscores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHighscores();
            }
        });

        // Ao clicar no botão de ver o perfil
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProfile();
            }
        });

    }



    /* Este método vai buscar os quartos à base de dados, e adiciona-os às arraylists, para serem mostrados na listview */
    public void getRooms() {

        // ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/rooms.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            // httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection successyy ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection successxx "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {
            JSONObject json_data = new JSONObject(result);
            listRooms = json_data.getJSONArray("list"); // Buscar array rooms
            success = (json_data.getInt("success")); // Buscar estado

            if (success == 1) {

                final ListView listview = (ListView) findViewById(R.id.listview); // Identifica a listview

                // Limpa a listview e as arraylists para preencher
                listview.setAdapter(null);
                roomNames.clear();
                gameTypes.clear();
                enterRoom.clear();
                roomOwners.clear();
                numberPeople.clear();
                roomIds.clear();
                roomPasswords.clear();


                for (int i = 0; i < listRooms.length(); i++) { // Percorre a lista de quartos
                    JSONObject r = listRooms.getJSONObject(i); // Vai buscar o elemento json atual (quarto atual)


                    // Apenas mostra, se existirem pessoas no quarto (quartos sem ninguém, não são mostrados)
                    if (r.getInt("players_in_room") != 0 || r.getInt("players_in_room_A") != 0 || r.getInt("players_in_room_B") != 0) {


                        roomNames.add(r.getString("name"));
                        roomIds.add(r.getInt("id_room"));
                        roomOwners.add(r.getInt("owner"));

                        if (r.getInt("open") == 0) {
                            enterRoom.add(R.drawable.key_icon);
                        } else {
                            enterRoom.add(R.drawable.enter_icon);
                        }

                        roomPasswords.add(r.getString("password"));

                        if (r.getInt("teams") == 0) {
                            gameTypes.add("Individually");
                            numberPeople.add(r.getInt("players_in_room") + "/" + r.getInt("num_people"));
                        } else {
                            gameTypes.add("Teams");
                            numberPeople.add(r.getInt("players_in_room_A") + "/" + r.getInt("num_people") + " - " + r.getInt("players_in_room_B") + "/" + r.getInt("num_people"));
                        }

                    }


                    listview.setAdapter(new ListViewAdapter(Rooms.this, importantUserId, roomNames, gameTypes, enterRoom, numberPeople, roomIds, roomPasswords, roomOwners, timer)); // Rooms é a classe em que estamos. roomNames, gameTypes e numberPeople são as ArrayList.
                    listview.setTextFilterEnabled(true);


                }

            } else {
                Toast.makeText(getBaseContext(), getString(R.string.no_rooms), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
            }


        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

    }




    public void showHighscores() {

        View view = (LayoutInflater.from(Rooms.this)).inflate(R.layout.modal_top10,null); // modal_top10 é o layout onde está os items que vai abrir na caixa de Diálogo
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Rooms.this);
        alertBuilder.setView(view);


        top10_list = (ListView) view.findViewById(R.id.top10_list);

        scores_list.clear();
        top10_list.setAdapter(null);




        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/getTop.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success11  ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8); // Foi mudado para UTF-8 por causa de um erro que acontecia no JSON
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success22 "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {

            JSONObject json_data = new JSONObject(result);


            // Buscar jogadores do jogo
            playersList = json_data.getJSONArray("list"); // Buscar array rooms

            for (int i = 0; i < playersList.length(); i++) { // Percorre a lista de jogadores
                JSONObject r = playersList.getJSONObject(i); // Vai buscar o elemento json atual (jogador atual)

                scores_list.add(r.getString("user"));
            }





        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }



        // Criar adaptador simples (apenas 1 string)
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, scores_list);

        // Atribuir o adaptador à listview
        top10_list.setAdapter(adapter);


        // O que acontece ao clicar no botão Cancel
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        Dialog dialog = alertBuilder.create();
        dialog.show(); // mostrar a caixa e diálogo

    }



    public void showProfile() {

        View view = (LayoutInflater.from(Rooms.this)).inflate(R.layout.modal_profile,null); // modal_profile é o layout onde está os items que vai abrir na caixa de Diálogo
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Rooms.this);
        alertBuilder.setView(view);


        TextView points = (TextView) view.findViewById(R.id.points);
        TextView gamesWon = (TextView) view.findViewById(R.id.games_won);
        TextView gamesLost = (TextView) view.findViewById(R.id.games_lost);
        TextView position = (TextView) view.findViewById(R.id.position);



        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.
        nameValuePairs.add(new BasicNameValuePair("userId", "" + importantUserId));

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/getProfile.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success11  ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8); // Foi mudado para UTF-8 por causa de um erro que acontecia no JSON
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success22 "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {

            JSONObject json_data = new JSONObject(result);

            if (json_data.getInt("position") == 0) {
                position.setText(getString(R.string.top10_position) + " -");
            } else {
                position.setText(getString(R.string.top10_position) + " " + json_data.getInt("position"));
            }

            points.setText(getString(R.string.points) + " " + json_data.getInt("points"));

            gamesLost.setText(getString(R.string.games_lost) + " " + json_data.getInt("gamesLost"));

            gamesWon.setText(getString(R.string.games_won) + " " + json_data.getInt("gamesWon"));


        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }





        // O que acontece ao clicar no botão Cancel
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        Dialog dialog = alertBuilder.create();
        dialog.show(); // mostrar a caixa e diálogo

    }



    public void showDialog() {

        View view = (LayoutInflater.from(Rooms.this)).inflate(R.layout.modal_layout,null); // modal_layout é o layout onde está os items que vai abrir na caixa de Diálogo
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Rooms.this);
        alertBuilder.setView(view);

        final EditText roomInput = (EditText) view.findViewById(R.id.roominput); // EditText para escrever nome do quarto
        final EditText passwordInput = (EditText) view.findViewById(R.id.passwordinput); // EditText para escrever password do quarto
        final RadioGroup group = (RadioGroup) view.findViewById(R.id.group); // Radio group para escolher tipo de jogo
        final NumberPicker np = (NumberPicker) view.findViewById(R.id.np); // Numberpicker para escolher nº jogadores
        final EditText nameA = (EditText) view.findViewById(R.id.teama); // Nome da equipa A
        final EditText nameB = (EditText) view.findViewById(R.id.teamb); // Nome da equipa B


        // Colocar o tipo de jogo Individual já pré definido
        final RadioButton indiv = (RadioButton) view.findViewById(R.id.individual);
        final RadioButton notindiv = (RadioButton) view.findViewById(R.id.notindividual);
        indiv.setChecked(true);
        team = 0;


        // Nome das equipas A e B, inicialmente estão invisiveis, até ser escolhido a opção de equipas
        nameA.setVisibility(View.GONE);
        nameB.setVisibility(View.GONE);


        np.setMinValue(2); // Mínimo de jogadores são 2
        np.setMaxValue(10); // Máximo de jogadores são 10

        np.setWrapSelectorWheel(true);


        // Ao selecionar uma opção do radiogroup
        // Radiogroup permite que apenas 1 das opções dos radiobuttons sejam selecionadas
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {


                if (indiv.isChecked() == true) { // Individual
                    nameA.setVisibility(View.GONE);
                    nameB.setVisibility(View.GONE);
                    team = 0;
                } else if (notindiv.isChecked() == true) { // Teams
                    nameA.setVisibility(View.VISIBLE);
                    nameB.setVisibility(View.VISIBLE);
                    team = 1;
                }

            }
        });


        // o que acontece ao clicar no botão Create
        alertBuilder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                createRoom(roomInput.getText().toString(), passwordInput.getText().toString(), team, np.getValue(), nameA.getText().toString(), nameB.getText().toString());

            }
        });

        // O que acontece ao clicar no botão Cancel
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        Dialog dialog = alertBuilder.create();
        dialog.show(); // mostrar a caixa e diálogo

    }



    /* Método que permite criar um novo quarto */
    public void createRoom(String roomName, String roomPassword, int teams, int roomNumPlayers, String nameTeamA, String nameTeamB) {

        if (roomName.equals("")) {
            Toast.makeText(getApplicationContext(), getString(R.string.fill_room_name), Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        } else if (teams == 1 && (nameTeamA.equals("") || nameTeamB.equals(""))) { // Se 1 dos nomes das equipas não tiver preenchido, no caso do jogo ser equipas
            Toast.makeText(getApplicationContext(), getString(R.string.fill_teams_name), Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        } else {


            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

            /* Dados que vão ser enviados para o php, para a base de dados */
            nameValuePairs.add(new BasicNameValuePair("roomName", roomName));
            nameValuePairs.add(new BasicNameValuePair("roomPassword", roomPassword));
            nameValuePairs.add(new BasicNameValuePair("teams", teams + ""));
            nameValuePairs.add(new BasicNameValuePair("roomNumPlayers", roomNumPlayers + ""));
            nameValuePairs.add(new BasicNameValuePair("nameTeamA", nameTeamA));
            nameValuePairs.add(new BasicNameValuePair("nameTeamB", nameTeamB));
            nameValuePairs.add(new BasicNameValuePair("owner", importantUserId + ""));

            /* Conectar à página web e envio do array dos dados */
            try {
                // código extremamente necessário para funcionar
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/create_room.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.e("pass 1", "connection success ");
            } catch (Exception e) {
                Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
                Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
            }

            /* Tratamento dos dados */
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
            } catch (Exception e) {
                Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            }

                /* Retorno dos dados */
            try {
                JSONObject json_data = new JSONObject(result);
                name = (json_data.getString("success")); /* Vai buscar ao json o campo "success" */
                idRoom = (json_data.getInt("id_room")); // Vai buscar ao json, o campo "id_room"

                if (name.equals("0")) {
                    Toast.makeText(getBaseContext(), getString(R.string.room_name_exists), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                } else if (name.equals("1")) {
                    Toast.makeText(getBaseContext(), getString(R.string.room_created), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).

                    // Escolher os beacons que estarão associados ao quarto
                    roomBeacons(teams, roomNumPlayers, idRoom);

                } else if (name.equals("2")) {
                    Toast.makeText(getBaseContext(), getString(R.string.room_open), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                }

            } catch (Exception e) {
                Log.e("Fail 3", result);
                Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            }

        }

    }




    // Este método permite associar beacons ao quarto
    public void roomBeacons(final int teams, final int roomNumPlayers, final int idRoom) {


        SystemRequirementsChecker.checkWithDefaultDialogs(this); // to enable bluetooth



        // Criar layout da modal
        View view = (LayoutInflater.from(Rooms.this)).inflate(R.layout.modal_beacons,null); // modal_layout é o layout onde está os items que vai abrir na caixa de Diálogo
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Rooms.this);
        alertBuilder.setView(view);



        // Identificar a listview e criar a arraylist
        final ListView room_beacons_list = (ListView) view.findViewById(R.id.room_beacons_list);
        final ArrayList<String> beacons_found = new ArrayList<String>();


        final Button findBeacons = (Button) view.findViewById(R.id.search_button);


        // Arraylist onde serão guardados os beacons que já foram encontrados (para evitar fazer várias consultas à base de dados)
        final ArrayList<String> beaconMacAddress = new ArrayList<String>();
        final ArrayList<String> beaconInOtherRoom = new ArrayList<String>();


        findBeacons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
                    @Override
                    public void onServiceReady() {
                        beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
                    }
                });

                findBeacons.setVisibility(View.GONE);

                beaconManager.setRangingListener(new BeaconManager.RangingListener() {

                    @Override
                    public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {

                        // Limpar arraylist e listview
                        room_beacons_list.setAdapter(null);
                        beacons_found.clear();


                        if (beacons.size() != 0) {

                            for (int i = 0; i < beacons.size(); i++) {
                                beacons_found.add("" + beacons.get(i));
                                // Se o beacon ainda não estiver guardado na base de dados, vai guardar
                                if (!beaconMacAddress.contains("" + beacons.get(i).getMacAddress()) && !beaconInOtherRoom.contains("" + beacons.get(i).getMacAddress())) {
                                    assignBeaconToRoom(beacons.get(i), idRoom, beaconMacAddress, beaconInOtherRoom);
                                    if (beaconInOtherRoom.size() > 0) { // Existem beacons que não foram associados porque já existem noutros quartos
                                        Toast.makeText(getBaseContext(), getString(R.string.beacons_assigned) + " " + beaconMacAddress.size() + " \n" + getString(R.string.already_assigned) + " " + beaconInOtherRoom.size(), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getBaseContext(), getString(R.string.beacons_assigned) + " " + beaconMacAddress.size() , Toast.LENGTH_SHORT).show();
                                    }
                                }
                        }


                    }

                    // Criar adaptador simples (apenas 1 string)
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, beacons_found);

                    // Atribuir o adaptador à listview
                    room_beacons_list.setAdapter(adapter);

                }
            }

            );


        }
    });




        // o que acontece ao clicar no botão Create
        alertBuilder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Dono do quarto entra no quarto
                // userEntersOnRoom(idRoom, importantUserId, teams, 1); // Adiciona o dono ao quarto na base de dados (sempre na equipa A)


                beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);

                // Caso não existam beacons associados ao quarto, apaga o quarto
                if (beaconMacAddress.size() == 0) {

                    deleteRoom(idRoom);

                } else { // Caso hajam beacons associados ao quarto

                    timer.cancel(); // Pára de atualizar os quartos


                    // Colocao o quarto ativo = 1, para outras pessoas poderem entrar
                    // Pois, o quarto já está criado e todos os beacons associados
                    makeRoomActive(idRoom);


                    Intent i = new Intent(getApplicationContext(), GameWaiting.class);
                    // nome da variável enviada no 1º argumento (utilizada para receber na outra atividade) e o valor é o 2º argumento
                    i.putExtra("roomId", idRoom); // envia o id do quarto para a nova atividade (GameWaiting)
                    i.putExtra("importantUserId", importantUserId);
                    i.putExtra("teams", teams); // 0 -> individual / 1 -> equipas
                    i.putExtra("numPlayersA", "1"); // Envia o nº de jogadores no quarto
                    i.putExtra("numPlayersB", "0"); // Não é utilizado pois é jogo individual
                    i.putExtra("maxPlayers", "" + roomNumPlayers); // Máximo de jogares permitido no quarto
                    startActivity(i);

                }


            }
        });


        Dialog dialog = alertBuilder.create();
        dialog.show(); // mostrar a caixa e diálogo

    }



    // Este método permite colocar o quarto ativo, ou seja, para poder aparecer no lobby
    public void makeRoomActive(int idRoom) {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

            /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("roomId", "" + idRoom));

            /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/room_active.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

            /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

                /* Retorno dos dados */
        try {
            JSONObject json_data = new JSONObject(result);

            int success = json_data.getInt("success");

        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

    }


    // Este método permite apagar um quarto da base de dados
    public void deleteRoom(int roomId) {
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("roomId", "" + roomId));

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/delete_room.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

            /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

            /* Retorno dos dados */
        try {
            JSONObject json_data = new JSONObject(result);
            name = (json_data.getString("success"));

            if (name.equals("1")) {

                Toast.makeText(getBaseContext(), getString(R.string.no_beacons_assigned), Toast.LENGTH_SHORT).show(); // Dados errados

            }

        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }
    }



    // Este método permite associar um beacon ao quarto
    public void assignBeaconToRoom(Beacon b, int roomId, ArrayList<String> beaconMacAddress, ArrayList<String> beaconInOtherRoom) {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("major", "" + b.getMajor()));
        nameValuePairs.add(new BasicNameValuePair("minor", "" + b.getMinor()));
        nameValuePairs.add(new BasicNameValuePair("roomId", "" + roomId));

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/assign_beacon.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

            /* Retorno dos dados */
        try {
            JSONObject json_data = new JSONObject(result);
            name = (json_data.getString("success")); /* Vai buscar ao json o campo "success" */

            if (name.equals("0")) { // Beacon não existe
                Toast.makeText(getBaseContext(), getString(R.string.beacon_not_exist), Toast.LENGTH_SHORT).show();
            } else if (name.equals("1")) { // Beacon existe e não está associado a nenhum quarto
                beaconMacAddress.add("" + b.getMacAddress());
            } else if (name.equals("2")) { // Beacon existe mas está associado a outro quarto
                beaconInOtherRoom.add("" + b.getMacAddress());
            }

        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

    }


    /* @Override
    protected void onResume() {

        super.onResume();
        SystemRequirementsChecker.checkWithDefaultDialogs(this); // to enable bluetooth

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {

            @Override
            public void onServiceReady() {

                beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);

            }

        });

    } */



    /* Retirar ação do botão de voltar para trás */
    @Override
    public void onBackPressed() {

    }


}