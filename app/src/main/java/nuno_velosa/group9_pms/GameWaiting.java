package nuno_velosa.group9_pms;

// Fazer os imports

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static android.widget.RelativeLayout.*;


public class GameWaiting extends Activity {

    Serializable valor;
    Serializable numPlayersA, numPlayersB, maxPlayers, playingInTeams;


    boolean doOnStop = true;


    ArrayList<String> playerNameA = new ArrayList();
    ArrayList<String> playerNameB = new ArrayList();
    ArrayList<Integer> playerIdA = new ArrayList();
    ArrayList<Integer> playerIdB = new ArrayList();
    ArrayList<String> playerImageA = new ArrayList();
    ArrayList<String> playerImageB = new ArrayList();


    // Variáveis para aceder à base de dados
    InputStream is=null;
    String result=null;
    String line=null;
    JSONArray playersList = null, playersAList = null, playersBList = null; // Onde ficarão armazenados os jogadores



    int success, num_people, owner, teams, id_room, players_in_room, players_in_room_A, players_in_room_B;
    String name, nameA, nameB;


    Timer timer;


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    // Método onCreate() é chamado ao iniciar a aplicação.
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Escolher o layout (ficheiro xml) que vai abrir ao iniciar aplicação, neste caso activity_main.xml.
        setContentView(R.layout.gamewaiting);

        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Buscar a Intent
        Intent intent = getIntent();

        // Buscar a variável
        // Irá guardar na variável valor, a variável que foi enviada com a identificação "roomId"
        valor = intent.getSerializableExtra("roomId");
        // Vai buscar id do utilizador que fez login
        Serializable id = intent.getSerializableExtra("importantUserId");
        importantUserId = (Integer) id;
        playingInTeams = intent.getSerializableExtra("teams");
        numPlayersA = intent.getSerializableExtra("numPlayersA");
        numPlayersB = intent.getSerializableExtra("numPlayersB");
        maxPlayers = intent.getSerializableExtra("maxPlayers");



        // Listas com os jogadores que estão no quarto
        final ListView list_teamA = (ListView) findViewById(R.id.team_a);
        final ListView list_teamB = (ListView) findViewById(R.id.team_b);

        timer = new Timer();

        getPlayers((Integer) valor, list_teamA, list_teamB);


        // Criação do timer para atualizar os quartos de 5 em 5 segundos
        try {
            timer.scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {

                    runOnUiThread(new Runnable() {
                        public void run() {
                            getPlayers((Integer) valor, list_teamA, list_teamB); // Preenche os jogadores que estão no quarto
                        }
                    });

                }
            }, 0, 2000);   // 2000 milisegundos = 2 segundos
        } catch (IllegalStateException e) {
            Log.e("timer", e.getMessage().toString());
        }



        int playIT = (Integer) playingInTeams;
        String nA, nB, mP;
        nA = (String) numPlayersA;
        nB = (String) numPlayersB;
        mP = (String) maxPlayers;

        if (playIT == 0) { // Jogo individual
            if (nA.equals(mP)) {
                // Jogo começa automaticamente, quarto está cheio
                startGame((Integer) playingInTeams, list_teamA, list_teamB, false);
            }
        } else if (playIT == 1) { // Jogo de equipas
            if (nA.equals(mP) && nB.equals(mP)) {
                // Jogo começa automaticamente, ambas as equipas estão cheias
                startGame((Integer) playingInTeams, list_teamA, list_teamB, false);
            }
        }


    }




    /* Este método vai buscar os jogadores à base de dados, e adiciona-os às arraylists, para serem mostrados na listview */
    public void getPlayers(int roomId, ListView list_teamA, ListView list_teamB) {


        // Limpar listviews e arrays
        list_teamA.setAdapter(null);
        list_teamB.setAdapter(null);
        playerNameA.clear();
        playerNameB.clear();
        playerIdA.clear();
        playerIdB.clear();
        playerImageA.clear();
        playerImageB.clear();



        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.
        nameValuePairs.add(new BasicNameValuePair("roomId", roomId + "")); // Envia para o ficheiro php

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/players.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try {

            JSONObject json_data = new JSONObject(result);
            success = (json_data.getInt("success")); // Buscar estado
            name = (json_data.getString("name")); // Buscar nome do quarto
            num_people = (json_data.getInt("num_people")); // Buscar capacidade do quarto
            owner = (json_data.getInt("owner")); // Buscar dono do quarto
            teams = (json_data.getInt("teams")); // Buscar tipo de jogo (teams=0 no caso de individual e teams=1 no caso de equipas)


            // Alterar os dados do quarto (nome, nº pessoas, dono, ...)

            TextView name_of_room = (TextView) findViewById(R.id.name_of_room);
            TextView teama_list = (TextView) findViewById(R.id.teama_list);
            TextView teamb_list = (TextView) findViewById(R.id.teamb_list);

            name_of_room.setText(name);

            final Button start_game = (Button) findViewById(R.id.startgame);
            final Button leave_game = (Button) findViewById(R.id.leavegame);


            // Ao sair do quarto
            leave_game.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    leave_game.setEnabled(false);
                    leaveRoom((Integer) valor);
                }
            });



            final ListView listviewA = (ListView) findViewById(R.id.team_a); // Identifica a listview
            final ListView listviewB = (ListView) findViewById(R.id.team_b); // Identifica a listview



            // Ao clicar em start game (começar o jogo)
            start_game.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    start_game.setEnabled(false);
                    /* Não começa logo mas sim espera pelo timer     */
                    startGame(teams, listviewA, listviewB, false);

                }
            });



            if (owner == importantUserId) { // Se for o dono, mostra o botão start
                start_game.setVisibility(View.VISIBLE);
            } else { // Se não for o dono, não mostra o botão start
                start_game.setVisibility(View.GONE);
            }


            if (success == 1) {

                if (teams == 0) { // Jogo individual


                    listviewB.setVisibility(View.GONE);
                    teamb_list.setVisibility(View.GONE);
                    RelativeLayout.LayoutParams mParam = new RelativeLayout.LayoutParams((int)(RelativeLayout.LayoutParams.WRAP_CONTENT),(int)(500));
                    mParam.setMargins(0, 100, 0, 0);
                    listviewA.setLayoutParams(mParam);


                    playersList = json_data.getJSONArray("list"); // Buscar array rooms
                    players_in_room = (json_data.getInt("players_in_room")); // Buscar nº jogadores

                    teama_list.setText(players_in_room + "/" + num_people);


                    for (int i = 0; i < playersList.length(); i++) { // Percorre a lista de quartos
                        JSONObject r = playersList.getJSONObject(i); // Vai buscar o elemento json atual (quarto atual)

                        playerNameA.add(r.getString("name"));
                        playerIdA.add(r.getInt("id_user"));
                        playerImageA.add(r.getString("image"));

                    }


                    // Este último parâmetro (1) indica qual o layout que será mostrado
                    // No caso do layout do Quarto é 1, no caso do layout do Jogo é 2
                    listviewA.setAdapter(new RoomAdapter(GameWaiting.this, importantUserId, playerNameA, playerIdA, playerImageA, 1, new ArrayList<Integer>()));
                    listviewA.setTextFilterEnabled(true);


                    // Jogo começa automaticamente em todos os telemóveis (sincronizados +-)
                    if (players_in_room == num_people || json_data.getInt("active") == 0) { // No jogo individual, se o quarto estiver cheio ou clicarem no botão start (active=0)
                        timer.cancel(); // Pára de atualizar os jogadores
                        startGame((Integer) playingInTeams, list_teamA, list_teamB, true);
                    }

                } else if (teams == 1) { // Jogo de equipas

                    playersAList = json_data.getJSONArray("listA"); // Buscar array rooms
                    playersBList = json_data.getJSONArray("listB"); // Buscar array rooms
                    players_in_room_A = (json_data.getInt("players_in_room_A")); // Buscar nº jogadores da equipa A
                    players_in_room_B = (json_data.getInt("players_in_room_B")); // Buscar nº jogadores da equipa B
                    nameA = (json_data.getString("nameA")); // Buscar nome da equipa A
                    nameB = (json_data.getString("nameB")); // Buscar nome da equipa B


                    teama_list.setText(nameA + " " + players_in_room_A + "/" + num_people); // Colocar nome das equipas e jogadores que stão a jogar
                    teamb_list.setText(nameB + " " + players_in_room_B + "/" + num_people);



                    for (int i = 0; i < playersAList.length(); i++) { // Percorre a lista de quartos
                        JSONObject r = playersAList.getJSONObject(i); // Vai buscar o elemento json atual (quarto atual)

                        playerNameA.add(r.getString("name"));
                        playerIdA.add(r.getInt("id_user"));
                        playerImageA.add(r.getString("image"));

                    }

                    for (int i = 0; i < playersBList.length(); i++) { // Percorre a lista de quartos
                        JSONObject r = playersBList.getJSONObject(i); // Vai buscar o elemento json atual (quarto atual)

                        playerNameB.add(r.getString("name"));
                        playerIdB.add(r.getInt("id_user"));
                        playerImageB.add(r.getString("image"));

                    }


                    listviewA.setAdapter(new RoomAdapter(GameWaiting.this, importantUserId, playerNameA, playerIdA, playerImageA, 1, new ArrayList<Integer>()));
                    listviewA.setTextFilterEnabled(true);

                    listviewB.setAdapter(new RoomAdapter(GameWaiting.this, importantUserId, playerNameB, playerIdB, playerImageB, 1, new ArrayList<Integer>()));
                    listviewB.setTextFilterEnabled(true);


                    // Jogo começa automaticamente em todos os telemóveis (sincronizados +-)
                    if ((players_in_room_A == num_people && players_in_room_B == num_people) || json_data.getInt("active") == 0) { // No jogo de equipas, se as equipas estiverem cheias ou o dono clicar start (active=0)
                        timer.cancel(); // Pára de atualizar os jogadores
                        startGame((Integer) playingInTeams, list_teamA, list_teamB, true);
                    }


                }

            } else {
                Toast.makeText(getBaseContext(), getString(R.string.no_rooms), Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
            }


        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

    }



    // Método para começar o jogo
    public void startGame(int teams, ListView lA, ListView lB, boolean irParaCountdown) {

        if (teams == 0 && lA.getAdapter().getCount() < 2) { // Se existirem menos de 2 jogadores (individual)
                Toast.makeText(getBaseContext(), getString(R.string.game_must), Toast.LENGTH_SHORT).show(); // Dados errados
        } else if (teams == 1 && lA.getAdapter().getCount() != lB.getAdapter().getCount()) { // Se as equipas não estiverem equilibradas (com mesmo nº de elementos) (equipas)
            Toast.makeText(getBaseContext(), getString(R.string.teams_must), Toast.LENGTH_SHORT).show(); // Dados errados
        } else {
            doOnStop = false;

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
            nameValuePairs.add(new BasicNameValuePair("roomId", "" + (Integer) valor));

        /* Conectar à página web e envio do array dos dados */
            try {
                // código extremamente necessário para funcionar
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/create_game.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.e("pass 1", "connection success ");
            } catch (Exception e) {
                Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
                Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
            }

        /* Tratamento dos dados */
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
            } catch (Exception e) {
                Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            }

            /* Retorno dos dados */
            try {
                JSONObject json_data = new JSONObject(result);
                name = (json_data.getString("success")); /* Vai buscar ao json o campo "success" */

                // if (name.equals("0")) { // ERRO
                //     Toast.makeText(getBaseContext(), "Game already exists!", Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
                // } else {
                if (irParaCountdown) {
                    Intent i = new Intent(getApplicationContext(), Countdown.class); // Vai para uma nova atividade (vai para o jogo)
                    i.putExtra("importantUserId", importantUserId); // Enviar id do utilizador para a atividade seguinte
                    i.putExtra("roomId", (Integer) valor);
                    i.putExtra("teams", teams); // Envia o tipo de jogo
                    startActivity(i);
                }
                // }

            } catch (Exception e) {
                Log.e("Fail 3", result);
                Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            }

        }

    }



    // Método para sair do quarto
    public void leaveRoom(int roomId) {

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("id_user", "" + importantUserId));
        nameValuePairs.add(new BasicNameValuePair("roomId", "" + roomId));

        /* Conectar à página web e envio do array dos dados */
        try {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/exit_room.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        } catch (Exception e) {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        } catch (Exception e) {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

            /* Retorno dos dados */
        try {
            JSONObject json_data = new JSONObject(result);
            success = (json_data.getInt("success")); /* Vai buscar ao json o campo "success" */

            if (success == 1) { // Sai do quarto
                Intent i = new Intent(getApplicationContext(), Rooms.class); // Vai para uma nova atividade (volta para a lista de quartos)
                i.putExtra("importantUserId", importantUserId); // Enviar id do utilizador para a atividade seguinte
                startActivity(i);
            }

        } catch (Exception e) {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

    }



    /* Retirar ação do botão de voltar para trás */
    @Override
    public void onBackPressed() {

    }


    // Quando o utilizador sai da aplicação (desliga) ou coloca-a em segundo plano
    public void onStop() {
        // Ao mudar de atividade, chama o onStop() mas doOnStop estará false, logo não sairá do quarto
        if (doOnStop) { // Se for para fazer o onStop, sai do quarto
            leaveRoom((Integer) valor);
        }
        super.onStop();
    }




}