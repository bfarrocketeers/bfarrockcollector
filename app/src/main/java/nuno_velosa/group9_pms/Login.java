package nuno_velosa.group9_pms;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

// import com.google.api.client.http.HttpResponse;


public class Login extends Activity {


    // Variável necessária em todas as atividades (saber o id do utilizador presente)
    int importantUserId;


    // Variáveis
    EditText username, password;
    String usern, passw, name;
    InputStream is=null;
    String result=null;
    String line=null;
    Button login, register;


    // Login com Facebook
    private LoginButton loginButton;
    private CallbackManager callbackManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Facebook
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        /* Método chamado ao iniciar a app */
        setContentView(R.layout.main_menu); /* Primeiro layout a ser chamado */


        loginButton = (LoginButton) findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {


                Bundle params = new Bundle();
                params.putString("fields", "id,email,gender,cover,picture.type(large)");
                new GraphRequest(AccessToken.getCurrentAccessToken(), "me", params, HttpMethod.GET,
                        new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                if (response != null) {
                                    try {
                                        JSONObject data = response.getJSONObject();
                                        if (data.has("picture")) {
                                            String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                            // Log.e("IMAGEEM", profilePicUrl);

                                            // Bitmap profilePic= BitmapFactory.decodeStream(profilePicUrl.openConnection().getInputStream());
                                            // mImageView.setBitmap(profilePic);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }).executeAsync();



                Log.e("info",
                        "User ID: "
                                + loginResult.getAccessToken().getUserId()
                                + "\n" +
                                "Auth Token: "
                                + loginResult.getAccessToken().getToken()
                );


                Profile profile = Profile.getCurrentProfile();
                Log.e("ProfileDataNameF", "--" + profile.getFirstName());
                Log.e("ProfileDataNameL", "--" + profile.getLastName());
                // Log.e("Image URI", "--" + profile.getLinkUri());

                // Registar na base de dados
                registerFacebook(profile.getFirstName() + " " + profile.getLastName(), loginResult.getAccessToken().getUserId());



            }

            @Override
            public void onCancel() {
                Log.e("info", "Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e("info", "Login attempt failed.");
            }
        });



        // Remover rotação da app (não ficar na horizontal)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Colocar a app fullscreen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        login = (Button) findViewById(R.id.login); /* Vai buscar o botão com id login */
        register = (Button) findViewById(R.id.register); /* Vai buscar o botão com id register */
        username = (EditText) findViewById(R.id.username); /* Vai buscar a caixa de texto com id username */
        password = (EditText) findViewById(R.id.password); /* Vai buscar a caixa de texto com id password */

        login.setOnClickListener(new View.OnClickListener() { /* Ação ao clicar no botão do login */
            @Override
            public void onClick(View view) {

                login();

            }
        });


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), Register.class);
                startActivity(i);





              }
        });



    }




    /* Adiciona na base de dados a conta com facebook */
    public void registerFacebook(String name, String facebookId) {


        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

        /* Dados que vão ser enviados para o php, para a base de dados */
        nameValuePairs.add(new BasicNameValuePair("username", facebookId)); // adiciona ao array no campo "username" a variável susername.
        nameValuePairs.add(new BasicNameValuePair("password", "")); // adiciona ao array no campo "password" a variável spassword.
        nameValuePairs.add(new BasicNameValuePair("name", name)); // adiciona ao array no campo "name" a variável sname

        /* Conectar à página web e envio do array dos dados */
        try
        {
            // código extremamente necessário para funcionar
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/register.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            Log.e("pass 1", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
        }

        /* Tratamento dos dados */
        try
        {
            BufferedReader reader = new BufferedReader (new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }

        /* Retorno dos dados */
        try
        {
            JSONObject json_data = new JSONObject(result);
            importantUserId = (json_data.getInt("id_user")); // Vai buscar ao json o campo "id_user"

            Intent i = new Intent(getApplicationContext(), Rooms.class);
            i.putExtra("importantUserId", importantUserId);
            startActivity(i);

            // Toast.makeText(getBaseContext(), getString(R.string.welcome) + sname + "!", Toast.LENGTH_SHORT).show();

        }
        catch(Exception e)
        {
            Log.e("Fail 3", result);
            Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
        }


    }




    /* Retirar ação do botão de voltar para trás */
    @Override
    public void onBackPressed() {

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }



    public void login() {

        /* Vai buscar os valores que estão nas caixas de texto, e converte para string (pois valores são Editable) */
        usern = username.getText().toString();
        passw = password.getText().toString();

        if (usern.equals("") || passw.equals("")) { // Se o username ou a password não estiverem preenchidos
            Toast.makeText(getBaseContext(), "Please, fill your username and password.", Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).
        } else {

            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(); // Cria uma array do tipo NameValuePair.

            /* Dados que vão ser enviados para o php, para a base de dados */
            nameValuePairs.add(new BasicNameValuePair("username", usern)); // adiciona ao array no campo "username" a variável usern.
            nameValuePairs.add(new BasicNameValuePair("password", passw)); // adiciona ao array no campo "password" a variável passw.

            /* Conectar à página web e envio do array dos dados */
            try {
                // código extremamente necessário para funcionar
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://mars.tigerwhale.com/login.php"); // Ficheiro php que vai receber os valores (ver o ficheiro abaixo).
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.e("pass 1", "connection success ");
            } catch (Exception e) {
                Log.e("Fail 1", e.toString()); // Caso falhe, escreve na console qual foi o erro.
                Toast.makeText(getApplicationContext(), "Invalid IP Address", Toast.LENGTH_LONG).show(); // Caso falhe, aparece uma mensagem a dizer "Invalid IP Adress".
            }

            /* Tratamento dos dados */
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
                Log.e("pass 2", "connection success "); // Caso não falhe, escreve na console "connection success".
            } catch (Exception e) {
                Log.e("Fail 2", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            }

            /* Retorno dos dados */
            try {
                JSONObject json_data = new JSONObject(result);
                name = (json_data.getString("name")); /* Vai buscar ao json o campo "name" */
                importantUserId = (json_data.getInt("id_user")); // Vai buscar ao json o campo "id_user"

                if (name.equals("0")) {

                    EditText usernameEdit = (EditText) findViewById(R.id.username);
                    EditText passwordEdit = (EditText) findViewById(R.id.password);
                    usernameEdit.setBackgroundResource(R.drawable.menu_button_style_red);
                    passwordEdit.setBackgroundResource(R.drawable.menu_button_style_red);
                    Toast.makeText(getBaseContext(), "Username or password wrong.", Toast.LENGTH_SHORT).show(); // Dados errados

                } else {

                    Intent i = new Intent(getApplicationContext(), Rooms.class); // Vai para uma nova atividade
                    i.putExtra("importantUserId", importantUserId); // Enviar id do utilizador para a atividade seguinte
                    startActivity(i);
                    Toast.makeText(getBaseContext(), "Welcome " + name + "!", Toast.LENGTH_SHORT).show(); // Caso funcione, vai dar o name do id que eu pedi (variavel name).

                }

            } catch (Exception e) {
                Log.e("Fail 3", result);
                Log.e("Fail 3", e.toString()); // Caso falhe, escreve na console qual foi o erro.
            }
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
