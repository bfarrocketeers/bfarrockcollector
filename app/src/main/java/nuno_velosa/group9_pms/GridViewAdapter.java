package nuno_velosa.group9_pms;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class GridViewAdapter extends BaseAdapter
{
    private ArrayList<String> imagemBeacon; // nomes das imagens dos beacons
    private ArrayList<String> nomeBeacon; // nomes dos beacons
    private ArrayList<Integer> userBeacon; // jogador que fez scan ao beacon
    private ArrayList<Integer> playersOfTheRoom; // todos os jogadores do jogo
    private ArrayList<Integer> colors; // 20 cores que serão atribuidas aos jogadores (para circular os beacons encontrados)
    private Activity activity;
    Context context;

    public GridViewAdapter(Context context, Activity activity, ArrayList<String> imagemBeacon, ArrayList<String> nomeBeacon, ArrayList<Integer> userBeacon, ArrayList<Integer> playersOfTheRoom, ArrayList<Integer> colors) {
        super();
        this.context = context;
        this.imagemBeacon = imagemBeacon;
        this.nomeBeacon = nomeBeacon;
        this.userBeacon = userBeacon;
        this.playersOfTheRoom = playersOfTheRoom;
        this.colors = colors;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return imagemBeacon.size();
    }

    @Override
    public String getItem(int position) {
        // TODO Auto-generated method stub
        return imagemBeacon.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public static class ViewHolder
    {
        public ImageView imgViewFlag;
        public TextView txtViewFlag;
    }





    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder view;
        LayoutInflater inflator = activity.getLayoutInflater();

        if(convertView==null)
        {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.game_item, null); // o que vai aparecer em cada elemento (neste caso apenas ImageView)

            // Elementos de cada item do gridview
            view.imgViewFlag = (ImageView) convertView.findViewById(R.id.beacon_image);
            view.txtViewFlag = (TextView) convertView.findViewById(R.id.beacon_name);

            convertView.setTag(view);
        }
        else
        {
            view = (ViewHolder) convertView.getTag();
        }


        // listFlag.get(position) é o nome da imagem (Ex.: img)
        // drawable é a pasta que vai buscar. (Ex.: R.drawable.img)
        int ImageResource = context.getResources().getIdentifier("" + imagemBeacon.get(position),"drawable", context.getPackageName());


        // Atualizar a informação dos elementos do gridview, através dos arrays
        view.imgViewFlag.setImageResource(ImageResource); // coloca a imagem no lugar do objeto cujo id é imageVievv.
        view.txtViewFlag.setText(nomeBeacon.get(position)); //  Substitui o textview

        // Se já estiver sido encontrado o beacon (Se o id do utilizador que encontrou for diferente de 0, significa que alguém apanhou)
        if (userBeacon.get(position) != 0) {

            int color = 0, pos = 0;


            if (playersOfTheRoom.contains(userBeacon.get(position))) {
                pos = playersOfTheRoom.indexOf(userBeacon.get(position));
                color = colors.get(pos);
            }


            //use a GradientDrawable with only one color set, to make it a solid color
            GradientDrawable border = new GradientDrawable();
            // border.setColor(0xFFFFFFFF); //white background
            border.setStroke(1, color); //black border with full opacity
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                convertView.setBackgroundDrawable(border);
            } else {
                convertView.setBackground(border);
            }

        }

        return convertView;
    }
}